# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :examtemp,
  ecto_repos: [Examtemp.Repo]

# Configures the endpoint
config :examtemp, Examtemp.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "hW51XfLh1oRXxgIft4Zt7XifNTfrQsEIR7AulThdajwSg8CFaC1m35qrW1P+b5+9",
  render_errors: [view: Examtemp.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Examtemp.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
